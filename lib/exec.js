async function main(cmd) {
  const exec = require('child_process').exec
  return new Promise((resolve, reject) => {
    exec(cmd, (error, stdout,stderr) => {
      if (error) reject(error)
      resolve(stdout ? stdout : stderr)
    })
  })
}

module.exports.cmd = main