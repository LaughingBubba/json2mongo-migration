const chalk = require('chalk')

async function main( db, collection ) {
  const collAry = await db.listCollections().toArray()
  const collections = await collAry.map(c => c.name)

  const present = await collections.includes(collection)

  if (present) {

    try {
      await db.collection(collection).drop()
      console.log(chalk.cyan(`Previous ${collection} collection dropped`))
    } catch (err) {
      console.log(chalk.cyan(`Unable to drop ${collection}, data may be overwritten`, err))
      throw new Error(err)
    }

  } else {
    console.log(chalk.cyan(`Collection ${collection} already dropped`))
  }
}

module.exports.run = main