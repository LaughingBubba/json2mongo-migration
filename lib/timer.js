const chalk = require('chalk')

var startTime
var endTime

function start() {
  startTime = new Date()
  console.log(chalk.black.bgWhiteBright(`Start: ${startTime}`))
}

function end() {
  endTime = new Date()
  var timeDiff = endTime - startTime // in ms
  var msec = timeDiff
  
  var hh = Math.floor(msec / 1000 / 60 / 60 )
  msec -= hh * 1000 * 60 * 60
  
  var mm = Math.floor(msec / 1000 / 60 )
  msec -= mm * 1000 * 60 

  var ss = Math.floor(msec / 1000 )
  msec -= ss * 1000 

  console.log(chalk.black.bgWhiteBright(`End: ${endTime} duration ${hh} hours, ${mm} mins, ${ss} secs`))
}

module.exports.start = start
module.exports.end = end