// General
exports.C_YES           = C_YES           = 'y'
exports.C_NO            = C_NO            = 'n'
exports.C_MIGRATION_DB  = C_MIGRATION_DB  = 'migration_db'
exports.C_TARGET_DB     = C_TARGET_DB     = 'target_db'

// CLI Options
exports.OPT_MONGO   = OPT_MONGO   = 'mongo'
exports.OPT_JSON    = OPT_JSON    = 'json'
exports.OPT_INPUT   = OPT_INPUT   = 'input'
exports.OPT_OUTPUT  = OPT_OUTPUT  = 'output'
exports.OPT_DROP    = OPT_DROP    = 'drop'

// Environment variables
exports.ENV_MODE    = ENV_MODE    = 'ENV_MODE'
exports.ENV_MONGO   = ENV_MOMGO   = 'MONGO_URI'
exports.ENV_INPUT   = ENV_INPUT   = 'INPUT_FOLDER'
exports.ENV_OUTPUT  = ENV_OUTPUT  = 'OUTPUT_FOLDER'
exports.ENV_MGR_DB  = ENV_MGR_DB  = 'MIGRATION_DB'
exports.ENV_TGT_DB  = ENV_TGT_DB  = 'TARGET_DB'