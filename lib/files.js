const glob = require('glob')

async function main(files) {
  if (files !== '') {
    return await glob.sync(files)
  }
}

module.exports.find = main