const { chain } = require('stream-chain')
const { parser } = require('stream-json')
const { streamValues } = require('stream-json/streamers/StreamValues')
const { streamArray } = require('stream-json/streamers/StreamArray')

async function countDocs(json) {

  return new Promise( async (resolve, reject) => {

    try {

      let count = 0
      
      const pipeline = chain([
        fs.createReadStream(json),
        parser({}),
        streamArray(),
        data => {
          ++count
        }
      ])
  
      pipeline.on('data', () => {})
      pipeline.on('error', err => { reject(err) })
      pipeline.on('end', () => {
        resolve(count)
      })

    } catch (err) {
      reject(err)
    }
  })
}

module.exports.countDocs = countDocs