// Splits an existing JSON file into X number of JSON files with an even number of documents

const chalk = require('chalk')
const yargs = require('yargs')

const fs          = require('fs')
const fsPromises  = require('fs').promises
const { chain } = require('stream-chain')
const take = require('stream-chain/utils/take')
const { parser } = require('stream-json')
const { streamValues } = require('stream-json/streamers/StreamValues')
const { streamArray } = require('stream-json/streamers/StreamArray')

const Timer = require('../lib/timer')
const Files = require('../lib/files') 

const argv = yargs
.usage(`Usage: $0 <command> [options]`)
.default('env', 'dev')
.default('json', null)
.default('prefix', null)
.default('split', 10)
.argv

main()

async function main() {

  const envPath = '.env.' + argv.env

  dotenv = require('dotenv').config({ path: envPath })
  if (dotenv.error) throw dotenv.error

  console.log(chalk.cyan(`Splitting ${chalk.yellow(argv.split)} times in environment ${chalk.yellow(argv.env.toUpperCase())}`))

  let statusOK = true

  const input_path = argv.json ? argv.json : dotenv.parsed.INPUT_FOLDER

  try {

    Timer.start()
    await process(argv.prefix, input_path, {split: argv.split, OUTPUT_FOLDER: dotenv.parsed.OUTPUT_FOLDER})

  } catch (error) {

    statusOK = false
    console.log(error)

  } finally {

    console.log(`First X, exraction finished OK ${statusOK}`)
    Timer.end()

  }

  async function process(prefix, input_path) {

    const stats = await fsPromises.lstat(input_path)

    if (stats.isDirectory()) {
      search = input_path + (prefix ? prefix : '') + `*.json`
      files = await Files.find(search)
      if (files) {
        for (file in files) {
          await splitJSON(files[file])
        }
      }
    }

    if (stats.isFile()) {
      await splitJSON(input_path)
    }

  }

  async function splitJSON(json, options) {

    let count = await countDocs(json)

    console.log(`${count} documents read for ${json} which will be split ${options.split} times`)

    let sampleSet = getBoundaries(count, options.split)

    await generateJSONSubset(json, sampleSet)

    Timer.end()

  }

  async function generateJSONSubset(json, sampleSet) {

    return new Promise( async (resolve, reject) => {

      try {

        let read = 0
        let docs = []
        let split = 0
        
        const pipeline = chain([
          fs.createReadStream(json),
          parser({}),
          streamArray(),
          data => {
            docs.push(data.value)
            ++read
            if (sampleSet.includes(read)) {
              ++split
              let outfile = `${json}.${split}.json.split`
              fs.writeFileSync(outfile, JSON.stringify(docs, null, 2))
              docs = []
            }
          }
        ])
    
        pipeline.on('data', () => {})
        pipeline.on('error', err => { reject(err) })
        pipeline.on('end', () => {
          resolve()
        })

      } catch (err) {
        reject(err)
      }
    })
  }

  function getBoundaries(size, split) {

    div = Math.round(size / split)
    rem = size % split

    let sampleSet = []
    
    let i = 1
    do {
      let x = i * div
      x >= 1 ? sampleSet.push(x) : null
      ++i
    }
    while (i < split)

    last = ((i * div) +  rem) > size ? size : ((i * div) +  rem)
    sampleSet.push(last)

    return sampleSet
  }

  async function countDocs(json) {

    return new Promise( async (resolve, reject) => {

      try {

        let count = 0
        
        const pipeline = chain([
          fs.createReadStream(json),
          parser({}),
          streamArray(),
          data => {
            ++count
          }
        ])
    
        pipeline.on('data', () => {})
        pipeline.on('error', err => { reject(err) })
        pipeline.on('end', () => {
          resolve(count)
        })

      } catch (err) {
        reject(err)
      }
    })
  }

}