const chalk = require('chalk')
const yargs = require('yargs')
const Timer = require('../lib/timer')
const Files = require('../lib/files') 
const { resolve } = require('path')
const { rejects } = require('assert')

const argv = yargs
.usage(`Usage: $0 <command> [options]`)
.default('env', 'dev')
.default('json', null)
.default('prefix', null)
.default('max', 10)
.argv

main()

async function main() {

  const envPath = '.env.' + argv.env

  dotenv = require('dotenv').config({ path: envPath })
  if (dotenv.error) throw dotenv.error

  console.log(chalk.cyan(`First ${chalk.yellow(argv.max)} in environment ${chalk.yellow(argv.env.toUpperCase())}`))

  let statusOK = true

  const input_path = argv.json ? argv.json : dotenv.parsed.INPUT_FOLDER

  try {

    Timer.start()
    await process(argv.prefix, input_path)

  } catch (error) {

    statusOK = false
    console.log(error)

  } finally {

    console.log(`First X, exraction finished OK ${statusOK}`)
    Timer.end()

  }

  async function process(prefix, input_path) {
    
    const { promises: fs} = require('fs')

    const stats = await fs.lstat(input_path)

    if (stats.isDirectory()) {
      search = input_path + (prefix ? prefix : '') + `*.json`
      files = await Files.find(search)
      if (files) {
        for (file in files) {
          await first_x(files[file])
        }
      }
    }

    if (stats.isFile()) {
      await first_x(input_path)
    }

  }

  async function first_x(json) {

    return new Promise( async (resolve, reject) => {

      const fs = require('fs')
      const { chain } = require('stream-chain')
      const take = require('stream-chain/utils/take')
      const { parser } = require('stream-json')
      const { streamValues } = require('stream-json/streamers/StreamValues')
      const { streamArray } = require('stream-json/streamers/StreamArray')

      const outfile = `${json}.${argv.max}.first`

      try {

        fs.writeFileSync(outfile, '[')

        let read = 0
        
        const pipeline = chain([
          fs.createReadStream(json),
          parser({}),
          streamArray(),
          // take(1),
          data => {
            ++read
            const value = data.value
            return read < argv.max ? JSON.stringify(value, null, 2) + ',' : read == argv.max ? JSON.stringify(value, null, 2) : null
          },
          fs.createWriteStream(outfile, {flags: 'a'})
        ])
    
        pipeline.on('data', () => {})
        pipeline.on('error', err => { reject(err) })
        pipeline.on('end', async () => {
          console.log(`${read} documents read for ${json}`)
          fs.writeFileSync(outfile,']', {flag: 'a+'})
          resolve()
        })

      } catch (err ) {
        reject(err)
      }
    })
  }

}