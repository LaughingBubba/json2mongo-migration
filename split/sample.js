// Extracts a random sampling of the source JSON file into a target file with a similar name.

const fs          = require('fs')
const fsPromises  = require('fs').promises

const chalk = require('chalk')
const yargs = require('yargs')

const { chain } = require('stream-chain')
const take = require('stream-chain/utils/take')
const { parser } = require('stream-json')
const { streamValues } = require('stream-json/streamers/StreamValues')
const { streamArray } = require('stream-json/streamers/StreamArray')

const Timer = require('../lib/timer')
const Files = require('../lib/files') 
const countDocs = require('../lib/countDocs') 

const argv = yargs
.usage(`Usage: $0 <command> [options]`)
.default('env', 'dev')
.default('json', null)
.default('prefix', null)
.default('pct', null)
.default('max', 10)
.argv

main()

async function main() {

  const envPath = '.env.' + argv.env

  dotenv = require('dotenv').config({ path: envPath })
  if (dotenv.error) throw dotenv.error

  console.log(chalk.cyan(`Random sampling in environment ${chalk.yellow(argv.env.toUpperCase())}`))

  let statusOK = true

  const input_path = argv.json ? argv.json : dotenv.parsed.INPUT_FOLDER

  try {

    Timer.start()
    await process(argv.prefix, input_path, {max: argv.max, pct: argv.pct, OUTPUT_FOLDER: dotenv.parsed.OUTPUT_FOLDER})

  } catch (error) {

    statusOK = false
    console.log(error)

  } finally {

    console.log(`Sampling finished OK ${statusOK}`)
    Timer.end()

  }

  async function process(prefix, input_path, options) {

    const stats = await fsPromises.lstat(input_path)

    if (stats.isDirectory()) {
      search = input_path + (prefix ? prefix : '') + `*.json`
      files = await Files.find(search)
      if (files) {
        for (file in files) {
          await randSample(files[file], options)
        }
      }
    }

    if (stats.isFile()) {
      await randSample(input_path, options)
    }

  }

  async function randSample(json, options) {

    let count = await countDocs(json)
    let sample = Math.round(options.pct ? count / options.pct : options.max)

    console.log(`${count} documents read of which ${sample} will be sampled from r ${json}`)

    let sampleSet = getSampleSet(sample, count)

    await generateSampleJSON(json, sampleSet)

    Timer.end()

  }

  async function generateSampleJSON(json, sampleSet) {

    return new Promise( async (resolve, reject) => {

      try {
        let outfile = `${json}.rand.json.rand`

        let read = 0
        fs.writeFileSync(outfile, '[')
        
        const pipeline = chain([
          fs.createReadStream(json),
          parser({}),
          streamArray(),
          data => {
            const value = data.value
            ++read
            return sampleSet.includes(read) ? sampleSet.indexOf(read) + 1 == sampleSet.length ? JSON.stringify(value, null, 2) : JSON.stringify(value, null, 2) + ',' : null
          },
          fs.createWriteStream(outfile, {flags: 'a'})
        ])
    
        pipeline.on('data', () => {})
        pipeline.on('error', err => { reject(err) })
        pipeline.on('end', () => {
          fs.writeFileSync(outfile, ']', {flag: 'a'})
          resolve()
        })

      } catch (err) {
        reject(err)
      }
    })
  }

  function getSampleSet(sampleSize, setSize) {

    let sampleSet = []
    
    let i = 1
    do {
      let rnd = randomNumber(1, setSize)
      sampleSet.push(rnd)
      ++i
    }
    while (i < sampleSize)

    return sampleSet.sort( (a,b) => { return a-b })

    function randomNumber(min, max) {
      min = Math.ceil(min)
      max = Math.floor(max)
      return Math.floor(Math.random() * (max - min +  1)) + min
    }
  }

}