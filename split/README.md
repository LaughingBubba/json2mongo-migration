# Splitting

Sometimes you want cut your monster JSON file down to smaller chuncks that a text editor can open.

Currenlty 3x forms of splitting are supported:

- First X number of documents
- Split the file X number of times
- Random sampling

This can be applied to a single JSON file or a collection of files in a directy with a spplied prefix.

## Pull the First x docs from a JSON file : first.docs.js

This utility the first 1 to x number of documents from a JSON file.

This process is relatively quick as it only does a single pass of the file.

A new file containing the sample documents will be created in the same directory as the source file and will have  `.X.first` appened to the source file name where `X` is the number of documents extracted.

If the output file already exists, it will be over written.

## Split JSON file x of number times : split.docs.js

This utility will split the JSON file a set number of times which is treated as the divisor of the total number of documents in the file. Each step count split will contain the resultant number of documents. 

A new file containing the sample documents will be created in the same directory as the source file and will have  `.X.split` appened to the source file name where `X` set count of the split. If the output file already exists, it will be over written.

NOTE: This requires 2 passes of the JSON file.

NOTE: Split files will not all be the same storage size as the size of each document will (potentially) vary.

## Get Random Sampling of x number or y% of documents : random.docs.js

This utiltiy will extract, at random, a set number of documents OR a percentage of the total number of documents in the file.

A new file containing the sample documents will be created in the same directory as the source file and will have  `.rand` appened to the source file name. If the output file already exists, it will be over written.

NOTE: This requires 2 passes of the JSON file.