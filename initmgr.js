// Initialise migration - generates .env.* file for later use

const fs          = require('fs')
const fsPromises  = require('fs').promises

const chalk = require('chalk')
const yargs = require('yargs')
const { MongoClient } = require('mongodb')

const cnst = require('./lib/constants')

const argv = yargs
.usage(`Usage: $0 <command> [options]`)
.demandOption(['mongo'])
.default('env', 'dev')
.default('prefix', null)
.default('input', './data/input/')
.default('output', './data/output/')
.default('working', cnst.C_MIGRATION_DB)
.default('target', cnst.C_TARGET_DB)
.argv


main()

async function main() {
  
  console.log(chalk.cyan(`Initialising ${chalk.yellow(argv.env.toUpperCase())} DotEnv file`))

  let dot = []
  var stats

  // Write yaml file???

  const options = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    ... (process.env.CERT_FILE && { tlsCAFile:process.env.CERT_FILE})
  }
  const client = await new MongoClient( argv.mongo, options)

  let statusOK = true

  dot.push(`${cnst.ENV_MODE}=${argv.env}`)
  dot.push(`${cnst.ENV_MGR_DB}=${argv.working}`)
  dot.push(`${cnst.ENV_TGT_DB}=${argv.target}`)

  try {

    // Check mongo URI
    await client.connect()
    const db = await client.db(argv.working)
    await client.db('admin').command({ ping: 1 })
    dot.push(`${cnst.ENV_MONGO}=${argv.mongo}`)

    // check input dir
    try {
      stats = fs.statSync(argv.input)
      if (!stats.isDirectory()) throw new Error('must be a directory')
      dot.push(`${cnst.ENV_INPUT}=${argv.input}`)
    } catch (err) {
      statusOK = false
      console.log(chalk.cyanBright(`Problem with --output : ${chalk.bgYellow(chalk.black(err))}`))
    }

    // check output dir
    try {
      stats = fs.statSync(argv.output)
      if (!stats.isDirectory()) throw new Error('must be a directory')
      dot.push(`${cnst.ENV_OUTPUT}=${argv.output}`)
    } catch (err) {
      statusOK = false
      console.log(chalk.cyanBright(`Problem with --output : ${chalk.bgYellow(chalk.black(err))}`))
    }

    await fsPromises.writeFile(`.env.${argv.env}`, dot.join(`\r\n`))

  } catch (err) {

    statusOK = false
    console.error( chalk.black.bgYellow( err ))

  } finally {

    await client.close()
    console.log(chalk.cyan('Make status:'), statusOK ? chalk.greenBright('OK') : chalk.yellow('Fail'))

  }
}